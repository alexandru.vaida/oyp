<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Servicii</title>
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <div class="container">
      <a href="index.html" class="navbar-brand">Getnic</a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          <a href="index.php" class="nav-link">Acasa</a>
          </li>
          <li class="nav-item">
            <a href="about.php" class="nav-link">Despre noi</a>
          </li>
          <li class="nav-item active">
            <a href="services.php" class="nav-link">Servicii
            </a>
          </li>
          <li class="nav-item">
            <a href="blog.php" class="nav-link">Portofoliu</a>
          </li>
          <li class="nav-item">
            <a href="contact.php" class="nav-link">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- PAGE HEADER -->
  <header id="page-header">
    <div class="container">
      <br><br>
      <div class="row">
        <div class="col-md-6 m-auto text-center">
          <h1>Serviciile noastre</h1>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <br>
<div class="row">
  <p>Am investit intr-o statie de preparare / productie de mixturi asfaltice, marca  SMA 40, care prepara toate tipurile de mixturi asfaltice prevazute in normativele romanesti si europene, atat pentru a ne produce materia prima pentru lucrarile proprii de amenajari drumuri, dar si pentru terti beneficiari.

      Calitatea si garantiile pe care le primiti, vor face ca lucrarile de drumuri, trotuare, sosele sau autostrazi sa devina sigure si lipsite de vicii.</p>
</div>
<div class="row">

    <p>Seful statiei de mixturi asfaltice, colegul nostru ing. GHITA GEORGIAN 0727.806.465 se va ingriji ca dumneavoastra sa nu duceti lipsa de consultanta si sprijin total in achizitionarea si livrarea produselor care se potrivesc cel mai bine lucrarilor pe care le aveti.</p>
  </div>
</div>
  <!-- SERVICES SECTION -->
  <section id="services" class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="card text-center">
            <div class="card-header bg-dark text-white">
              <h5>Descrierea fabricii de mixturi asfaltice de langa Cernavoda</h5>
            </div>
            <div class="card-body">
              <ul class="list-group">
                <li class="list-group-item">
                   MODEL: SMA
                </li>
                <li class="list-group-item">
                TIP: 40
                </li>
                <li class="list-group-item">
                   AN FABRICATIE: 2012
                </li>
                <li class="list-group-item">
                  AN PUNERE IN FUNCTIUNE: 2012
                </li>
                <li class="list-group-item">
                   PRODUCTIVITATE TEHNICA: 40 TONE / ORA
                </li>
                <li class="list-group-item">
                   PRODUCTIVITATE OPERATIONALA: 40  TONE / ORA
                  </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-center">
            <div class="card-header bg-dark text-white">
              <h5><br> Servicii oferite <br> <br></h5>
            </div>
            <div class="card-body">
              <ul class="list-group">
                <li class="list-group-item">
                    Constructii civile si industriale
                </li>
                <li class="list-group-item">
                    Retele distributie apa
                </li>
                <li class="list-group-item">
                    Constructii hidrotehnice
                </li>
                <li class="list-group-item">
                    Alimentari cu apa si canalizare
                </li>
                <li class="list-group-item">
                    Reparatii capitale la cladiri
                </li>
              </li>
              <li class="list-group-item">
                  Confectii metalice  
              </li>
            </li>
            <li class="list-group-item">
                Tamplarie Al. si P.V.C.
            </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-center">
            <div class="card-header bg-dark text-white">
              <h5> <br> ECHIPAMENTELE PRINCIPALE <br> </h5>
            </div>
            <div class="card-body">
              <ul class="list-group">
                <li class="list-group-item">
                    1 tanc bitum de 50 tone
                </li>
                <li class="list-group-item">
                    1 siloz filler de 50 tone
                </li>
                <li class="list-group-item">
                    uscator de agregate
                </li>
                <li class="list-group-item">
                    ciur vibrator cu 4 site
                </li>
 
                <li class="list-group-item">
                    dozatoare gravimetrice
                </li>
                <li class="list-group-item">
                    malaxor cu amestecare
                </li>
                <li class="list-group-item">
                    1 buncar de stocare
                </li>
                <li class="list-group-item">
                    cabina de comanda
                </li>
              </ul>
          
            </div>
          </div>
        </div>
        <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
      </div>
     
    </div>
  </section>

 

  <!-- FOOTER -->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Getnic
            <span id="year"></span></p>
        </div>
      </div>
    </div>
  </footer>


  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>

  <script>
    // Get the current year for the copyright
    $('#year').text(new Date().getFullYear());

    $(document).ready(function(){

$(window).scroll(function(){
  if($(this).scrollTop() > 40){
    $('#myBtn').fadeIn();
  } else{
    $('#mybtn').fadeOut();
  }
});

$("#myBtn").click(function(){
  $('html ,body').animate({scrollTop : 0},800);
});
});


  </script>
</body>

</html>