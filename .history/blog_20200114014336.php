<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Portofoliu</title>
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <div class="container">
      <a href="index.html" class="navbar-brand">Getnic</a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="index.html" class="nav-link">Acasa</a>
          </li>
          <li class="nav-item">
            <a href="about.html" class="nav-link">Despre noi</a>
          </li>
          <li class="nav-item">
            <a href="services.html" class="nav-link">Servicii</a>
          </li>
          <li class="nav-item active">
            <a href="blog.html" class="nav-link">Portofoliu</a>
          </li>
          <li class="nav-item">
            <a href="contact.html" class="nav-link">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  

  <!-- PAGE HEADER -->
  <header id="page-header">
    <div class="container">
<br>
      <div class="row">
        <div class="col-md-6 m-auto text-center">
          <h1>Proiectele realizate de noi pana acum</h1>
          <br>
        </div>
       
      </div>
    </div>
  </header>

<!-- Slideshow-->
  <section id="showcase">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item carousel1-image-1 active">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block text-right mb-5">
               
              </div>
            </div>
          </div>

          <div class="carousel-item carousel1-image-2">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block mb-5">
               
              </div>
            </div>
          </div>

          <div class="carousel-item carousel1-image-3">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block text-right mb-5">
                
              </div>
            </div>
          </div>
        </div>

        <a href="#myCarousel" data-slide="prev" class="carousel-control-prev">
          <span class="carousel-control-prev-icon"></span>
        </a>

        <a href="#myCarousel" data-slide="next" class="carousel-control-next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
    </section>

  <!-- BLOG SECTION -->
  <div class="container">
      <br><br><br>
      <div class="row">
        <div class="col-md-10 m-auto text-center">
            <ul class="list-group">
                <li class="list-group-item">
                  <i class="fas fa-check"></i>Confectionat si montat tamplarie P.V.C. Unitatea 1 Cernavoda
                </li>
                <li class="list-group-item">
                  <i class="fas fa-check"></i> Confectionat si montat tamplarie P.V.C. Unitatea 2 Cernavoda
                </li>
                <li class="list-group-item">
                  <i class="fas fa-check"></i> Retea distributie alimentare cu apa Eforie Nord
                </li>
                <li class="list-group-item">
                  <i class="fas fa-check"></i> Canalizare menajera si retea distributie apa Medgidia
                </li>
                <li class="list-group-item">
                  <i class="fas fa-check"></i> Piata agro-alimentara Cernavoda
                </li>
                <li class="list-group-item">
                    <i class="fas fa-check"></i> Reabilitare muzeu local Cernavoda
                  </li>
                  <li class="list-group-item">
                      <i class="fas fa-check"></i> Amenajare terenuri sportive
                    </li>
                    <li class="list-group-item">
                        <i class="fas fa-check"></i>R.K. gradinite si scoli Cernavoda
                      </li>
                      <li class="list-group-item">
                          <i class="fas fa-check"></i>Confectionat si montat tamplarie P.V.C. hotel HOLLYWOOD
                        </li>
              </ul>
        </div>
  
      </div>
      <br><br><br>
      <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
  </div>
  

  <!-- FOOTER -->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Getnic
            <span id="year"></span></p>
        </div>
      </div>
    </div>
  </footer>


  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>

  <script>
    // Get the current year for the copyright
    $('#year').text(new Date().getFullYear());

    $(document).ready(function(){

$(window).scroll(function(){
  if($(this).scrollTop() > 40){
    $('#myBtn').fadeIn();
  } else{
    $('#mybtn').fadeOut();
  }
});

$("#myBtn").click(function(){
  $('html ,body').animate({scrollTop : 0},800);
});
});


  </script>
</body>

</html>