  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
      crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
      crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
    <link rel="stylesheet" href="css/style.css">
    <title>Acasa</title>
  </head>

  <body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="container">
        <a href="index.html" class="navbar-brand">Getnic</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a href="index.html" class="nav-link">Acasa</a>
            </li>
            <li class="nav-item">
              <a href="about.html" class="nav-link">Despre noi</a>
            </li>
            <li class="nav-item">
              <a href="services.html" class="nav-link">Servicii</a>
            </li>
            <li class="nav-item">
              <a href="blog.html" class="nav-link">Portofoliu</a>
            </li>
            <li class="nav-item">
              <a href="contact.html" class="nav-link">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- SHOWCASE SLIDER -->
    <section id="showcase">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item carousel-image-1 active">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block text-right mb-5">
               
              </div>
            </div>
          </div>

          <div class="carousel-item carousel-image-2">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block mb-5">
               
              </div>
            </div>
          </div>

          <div class="carousel-item carousel-image-3">
            <div class="container">
              <div class="carousel-caption d-none d-sm-block text-right mb-5">
                
              </div>
            </div>
          </div>
        </div>

        <a href="#myCarousel" data-slide="prev" class="carousel-control-prev">
          <span class="carousel-control-prev-icon"></span>
        </a>

        <a href="#myCarousel" data-slide="next" class="carousel-control-next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
    </section>

    <!--HOME ICON SECTION  -->
    <section id="home-icons" class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-6 text-center">
            <i class="fas fa-info"></i><br>
            <h4>Servicii oferite</h4>
            <p1> <i>constructii civile si industriale <br>
              retele distributie apa  <br>
              constructii hidrotehnice <br>
              alimentari cu apa si canalizare   <br>
              reparatii capitale la cladiri <br>
              confectii metalice <br>
              tamplarie Al. si P.V.C.</i> </p1>
          </div>
          <div class="col-md-6 mb-6 text-center">
              <i class="fas fa-question"></i>
            <h4>De ce noi?</h4>
            <p>  <i> Filozofia Getnic este simpla: sa faca parte din viata de zi cu zi, <br> avand grija de dumneavoastra, oferindu-va totul pentru interior, <br> totul pentru exterior, de o calitate superioara si usor de folosit, care sa va transforme casa intr-un decor interesant, placut si primitor.</i></p>
          </div>
        </div>
      </div>
    </section>

    <!-- HOME HEADING SECTION -->
    <section id="home-heading" class="p-5">
      <div class="dark-overlay">
        <div class="row">
          <div class="col">
            <div class="container pt-5">
              <h1>Parteneri</h1>
           
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- INFO SECTION -->
    
      <div class="container">
        <br><br>
        <div class="row">
          <div class="col-md-6">
            <h3>Roportal</h3>
            <p> Roportal.ro pune laolalta informatii existente online cu cele care nu au fost pana acum puse pe internet intr-un format accesibil, usor de navigat. <br> De asemenea ne-am propus sa strabatem Romania in lung si in lat si sa verificam informatiile din online cu ceea ce exista pe "teren".</p>
            <a href="https://www.roportal.ro/" class="btn btn-outline-danger btn-lg">Viziteaza site-ul</a>
          </div>
          <div class="col-md-6">
            <img src="img/roportal.jpg" alt="" class="img-fluid">
          </div>
        </div>
        <br> <br>
        <div class="row">
              <div class="col-md-6">
                <h3>FireFox</h3>
                <p> Mozilla pune oamenii peste profit în tot ceea ce spunem, construim și facem. De fapt, există o fundație non-profit în centrul întreprinderii noastre.</p>
                <a href="https://www.mozilla.org/en-US/" class="btn btn-outline-danger btn-lg">Viziteaza site-ul</a>
              </div>
              <div class="col-md-6">
                <img src="img/firefox.jpg" alt="" class="img-fluid">
                <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
              </div>  
     
        </div>
        <br><br><br>
      </div>
         <!-- DataTales Example -->
         <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "step2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT  * FROM clienti";
$result = $conn->query($sql);

?>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nume</th>
                      <th>Prenume</th>
                      <th>Adresa</th>
                      <th>Telefon</th>
                     
                    </tr>
                  </thead>
                  
                  <tbody>
                    <tr>

                    <?php

       if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            ?>
                      <td><?php  echo  $row["nume"]    ?></td>
                      <td><?php  echo  $row["prenume"]    ?></td>
                      <td><?php  echo  $row["adresa"]  ?></td>
                      <td><?php  echo  $row["telefon"]    ?></td>
                    
                      
                 
                      
                    </tr>
                    
 <?php
        }
        
   
    } else {
    echo "0 results";
}
$conn->close();
       


 ?>                   
               

                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      
           

    <!-- FOOTER -->
    <footer id="main-footer" class="text-center p-4">
      <div class="container">
        <div class="row">
          <div class="col">
            <p>Getnic
              <span id="year"></span></p>
          </div>
        </div>
      </div>
    </footer>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script>
      // Get the current year for the copyright
      $('#year').text(new Date().getFullYear());

      // Configure Slider
      $('.carousel').carousel({
        interval: 6000,
        pause: 'hover'
      });

      // Lightbox Init
      $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
      });

      $(document).ready(function(){

$(window).scroll(function(){
  if($(this).scrollTop() > 40){
    $('#myBtn').fadeIn();
  } else{
    $('#mybtn').fadeOut();
  }
});

$("#myBtn").click(function(){
  $('html ,body').animate({scrollTop : 0},800);
});
});

    </script>
  </body>

  </html>