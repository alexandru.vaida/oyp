<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" />
  <link rel="stylesheet" href="css/style.css">
  <title>Despre noi</title>
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <div class="container">
      <a href="index.php" class="navbar-brand">Getnic</a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          <a href="index.php" class="nav-link">Acasa</a>
          </li>
          <li class="nav-item active">
            <a href="about.php" class="nav-link">Despre noi</a>
          </li>
          <li class="nav-item">
            <a href="services.php" class="nav-link">Servicii
            </a>
          </li>
          <li class="nav-item">
            <a href="blog.php" class="nav-link">Portofoliu</a>
          </li>
          <li class="nav-item">
            <a href="contact.php" class="nav-link">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- PAGE HEADER -->
  <!-- <header id="page-header">
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto text-center">
          <h1>Despre noi</h1>
        </div>
      </div>
    </div>
  </header> -->

  <!-- ABOUT SECTION -->
  <section id="about" class="py-3">
    <div class="container">
        <br><br><br>
      <div class="row">
    
        <div class="col-md-6">
          <h1>Ce facem noi</h1>
          <br>
          <p>Societatea Comerciala GETNIC CONSTRUCT SERV SRL Cernavoda, este o societate romaneasca cu capital integral privat, infiintata in anul 2004.</p>
          <p>
              Societatea Comerciala GETNIC CONSTRUCT SERV SRL Cernavoda, este o societate romaneasca cu capital integral privat, infiintata in anul 2004.
              Domeniul principal de activitate al societatii este reprezentat de constructiile civile si industriale, iar din anul 2005 s-a investit in achizitionarea de utilaje moderne pentru tamplarie de P.V.C. , din acest an devenind un important jucator pe piata tamplariei in urma unui parteneriat incheiat cu GEALAN ROMANIA.</p>
        <p>De la infiintare pâna în prezent s-a reusit crearea unei baze materiale care sa ofere autonomia deplina a fluxului de productie si cooptarea unor resurse umane capabile sa realizeze saltul calitativ si sa propulseze societatea în topul primelor societati din România.</p>
        <p>Achizitionarea unor utilaje de înalta performanta ,precum si personalul de înalta competenta si profesionalism fac din societatea noastra un excelent partener.</p>
            </div>
        <div class="col-md-6">
          <img src="img/aboutus1.jpg" alt="" class="img-fluid rounded-circle d-none d-md-block about-img">
        </div>
        <div class="row">
            <div class="col-md-12">
               <p>&nbsp&nbsp&nbspFilozofia GETNIC este simpla: sa faca parte din viata de zi cu zi, avand grija de dumneavoastra, oferindu-va totul pentru interior, &nbsp&nbsp&nbsptotul pentru exterior, de o calitate superioara si usor de folosit, care sa va transforme casa intr-un decor interesant, placut si &nbsp&nbsp&nbspprimitor.</p>
              <p>&nbsp&nbsp&nbspSistemul de management al calitatii implementat si mentinut in societatea noastra a fost certificat de SRAC - Certificat. Nr. &nbsp&nbsp&nbsp2596/1 - 2005 si Certificatul IQNet Nr. RO-0514 - 2005.</p>
            
              </div>
        </div>
      </div>
      <br><br><br>
      <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    </div>
  </section>

  

  <!-- FOOTER -->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Getnic
            <span id="year"></span></p>
        </div>
      </div>
    </div>
  </footer>


  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>

  <script>
    // Get the current year for the copyright
    $('#year').text(new Date().getFullYear());

    $('.slider').slick({
      infinite: true,
      slideToShow: 1,
      slideToScroll: 1
    });
    $(document).ready(function(){

$(window).scroll(function(){
  if($(this).scrollTop() > 40){
    $('#myBtn').fadeIn();
  } else{
    $('#mybtn').fadeOut();
  }
});

$("#myBtn").click(function(){
  $('html ,body').animate({scrollTop : 0},800);
});
});

  </script>
</body>

</html>